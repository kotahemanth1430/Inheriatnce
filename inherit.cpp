#include<iostream>
using namespace std;
class Room1{
    public:
    int a=20;
};
//Single level inheritance
class Room2:public Room1{
public:
int b=20;
void display1()
{
    cout<<"Sinlke inheritance: "<<(a*b)<<endl;
}
};
//Multilevel inheritance
class Room3:public Room2{
    public:
    int c=30;
    void display2()
    {
        cout<<"multilevel inheritance: "<<(b*c)<<endl;
    }
};

class Room4{
    public:
    int d=40;
};
//multiple inheritance
class Room5:public Room1,public Room4{
    public:
    int e=a+d;
    void display4()
    {
        cout<<"mutiple inheritance: "<<e<<endl;
    }
};
//hybrid
class Room6:public Room5{
    public:
    int f=60;
    void display5()
    {
        cout<<"Hybrid inheritance: "<<(e*f)<<endl;
    }
};
//herirachal inheritance
class Room7:public Room1{
    public:
    int g=70;
    void display6()
    {
        cout<<"Herirachal inheritance: "<<(a*g)<<endl;
    }
};
int main()
{
    Room2 obj1;
    Room3 obj2;
    Room5 obj4;
    Room6 obj5;
    Room7 obj3;
    obj1.display1();
    obj2.display2();
    obj3.display6();
    obj4.display4();
    obj5.display5();
}